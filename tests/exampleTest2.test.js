import examplePage from '../pages/examplePage.page';
import pomocnik from '../pages/helper';
const { expect } = require('chai');


describe('It will test search', () => {

before(()=> {
    pomocnik.prepareSystem();
})

beforeEach(() => {
    pomocnik.goToMainPage();
})

it('it will check weather for Poznań', ()=> {
    examplePage.findSomething('Poznań')
    browser.waitForVisible('.results-list',pomocnik.timeout);
    expect(browser.getText('.results-list .articles')).to.include('Poznań')
})

it('it will check weather for Polska', ()=> {
    examplePage.findSomething('Polska')
    browser.waitForVisible('.results-list',pomocnik.timeout);
    expect(browser.getText('.results-list .articles')).to.include('Polska')
})
})

describe('It will test something', () => {

before(()=> {
    pomocnik.prepareSystem();
})

beforeEach(() => {
    pomocnik.goToMainPage();
})

it('it will check weather for Poznań', ()=> {
    examplePage.findSomething('Poznań')
    browser.waitForVisible('.results-list',pomocnik.timeout);
    expect(browser.getText('.results-list .articles')).to.include('Poznań')
})

it('it will check weather for Polska', ()=> {
    examplePage.findSomething('Polska')
    browser.waitForVisible('.results-list',pomocnik.timeout);
    expect(browser.getText('.results-list .articles')).to.include('Polska')
})
})