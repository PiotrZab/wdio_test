import examplePage from '../pages/examplePage.page';
const { expect } = require('chai');

xdescribe('It will test Accuweather', () => {

beforeEach(()=> {
    browser.url('https://www.accuweather.com/');
})

it('Will test logo', ()=> {
    browser.waitForExist(examplePage.logo, 5000);
    browser.click(examplePage.logo);
    browser.pause(2000);
    expect(browser.isVisible(examplePage.logo)).to.be.true;
})

it('Will test search', ()=> {
    examplePage.findCity('Poznań');
    browser.pause(1000);
})
})