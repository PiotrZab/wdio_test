const path = require('path');
const fs = require('fs');
const TAKEN_PATH = 'log'

require('shelljs/global');

const VisualRegressionCompare = require('wdio-visual-regression-service/compare');

const {parse} = require('platform');


const getTests = (testsList) => testsList.map((suite) => `tests/${suite}.test.js`);

exports.config = {

    specs: getTests([
        'exampleTest2'
        // 'example'
    ]),
    // Patterns to exclude.
    exclude: [],
    maxInstances: 10,
    capabilities: [
        {
            maxInstances: 1,
            browserName: 'chrome',
            chromeOptions: {
            }
        }// ,
        /* {
          browserName: 'firefox',
        },
        {
          browserName: 'phantomjs',
          'phantomjs.binary.path': require('phantomjs').path,
        }*/
    ],
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'error',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Set a base URL in order to shorten url command calls. If your url parameter starts
    // with "/", then the base url gets prepended.
    baseUrl: 'www.google.com',
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    plugins: {
        'wdio-screenshot': {}
    },
    services: [
        'selenium-standalone'
    ],
    // Options for selenium-standalone
    // Path where all logs from the Selenium server should be stored.
    seleniumLogs: 'logs/',
    port: 35357,
    seleniumArgs: {
        seleniumArgs: ['-port', '35357']
    },
    // execArgv: ['--max_old_space_size=4096', '--optimize_for_size', '--stack_size=4096'],
    // execArgv: ['--trace_gc', '--trace_gc_verbose'],
    framework: 'mocha',
  // reporters: ['teamcity'],
    reporters: ['json'],
    reporterOptions: {
        outputDir: path.join(TAKEN_PATH),
        filename: 'report',
        combined: true
    },
    waitforTimeout: 50000,
    mochaOpts: {
        ui: 'bdd',
        timeout: 5000000,
        compilers: [
            'js:babel-register'
        ]
    }
};