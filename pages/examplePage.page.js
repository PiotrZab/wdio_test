class ExamplePage {
constructor() {
    this.locator = '#home';
    this.logo = '#header-logo';
    this.search = '#findcity';
    this.searchField = '#s';
    this.searchButton = '.bt-go';
}

findSomething(zmienna) {
    browser.waitForExist('#findcity',5000);
    browser.click('#findcity');
    browser.setValue('#s',zmienna);
    browser.click('[type="button"].bt-go');
}

}

export default new ExamplePage();