class Helper {
constructor() {
    this.consentOk = '.fc-button-consent';
    this.dialogContainer = '.fc-dialog-container';
    this.timeout = 5000;
}

prepareSystem() {
    browser.url('https://www.accuweather.com/');
    browser.waitForVisible(this.dialogContainer, this.timeout);
    browser.click(this.consentOk);
    browser.waitForVisible(this.dialogContainer, this.timeout, true);
    browser.waitForVisible('#eu-cookie-notify', this.timeout);
    browser.click('.continue');
}

goToMainPage() {
    browser.url('https://www.accuweather.com/');
    browser.waitForExist('#header-logo-search', this.timeout);
}

}

export default new Helper();